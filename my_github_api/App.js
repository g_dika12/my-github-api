/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import type { Node } from 'react';
import React from 'react';
import Main from './src/Main';


const App: () => Node = () => {
 
  return (
    <Main />
  );
};

export default App;
