import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, FlatList } from 'react-native'

const Main = () => {
    const [data, setData] = useState(null)
    const getApi = async () => {
        axios.get('https://api.github.com/users/GuitherezDika/repos')
            .then((result) => {
                getProjectTitle(result.data)
            })
            .catch((err) => console.log(JSON.stringify(err.message)))
    }
    useEffect(() => {
        getApi()
    }, [])

    const getProjectTitle = (data) => {
        const newArray = []
        for (var i = 0; i < data.length; i++) {
            newArray.push(data[i].name)
        }
        setData(newArray)
    }
    // console.log('**',data)
    return (
        <View style={{ padding: 16, flex: 1, backgroundColor: "#ebeff5" }}>
            <FlatList
                data={data}
                keyExtractor={item => item.id}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) => {
                    console.log(item)
                    return (
                        <View style={styles.card}>
                            <Text style={{fontSize:14, color:'black'}}>{item}</Text>
                        </View>
                    )
                }} />
        </View>
    )
}

export default Main
const styles = StyleSheet.create({
    card: {
        width: '100%',
        borderWidth: 1,
        borderColor: '#08a833',
        borderRadius: 8,
        height: 44,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:8
    }
})